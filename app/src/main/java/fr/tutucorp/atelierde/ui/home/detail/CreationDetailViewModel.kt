package fr.tutucorp.atelierde.ui.home.detail

import androidx.lifecycle.ViewModel
import fr.tutucorp.atelierde.data.MainRepository
import fr.tutucorp.atelierde.ui.home.detail.model.Creation

class CreationDetailViewModel : ViewModel() {
    fun getCreation(id: String): Creation {
        return MainRepository.getInstance().getCreation(id = id)!!
    }
}