package fr.tutucorp.atelierde.ui.home

import android.animation.TimeInterpolator
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.DecelerateInterpolator
import android.widget.CompoundButton
import android.widget.HorizontalScrollView
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.imageLoader
import coil.load
import coil.request.ImageRequest
import com.google.android.material.chip.Chip
import fr.tutucorp.atelierde.R
import fr.tutucorp.atelierde.databinding.ChipsItemBinding
import fr.tutucorp.atelierde.databinding.CreationItemBinding
import fr.tutucorp.atelierde.ui.home.model.CreationItem
import kotlinx.coroutines.delay

internal class CreationRecyclerViewAdapter(
    context: Context,
    data: List<CreationItem> = emptyList(),
    private val clickListener: (CreationItem, CreationItemBinding) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val originalData: MutableList<CreationItem> = data.toMutableList()
    private val data: MutableList<CreationItem> = data.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            0 -> ChipsViewHolder(ChipsItemBinding.inflate(inflater, parent, false))
            1 -> CardViewHolder(CreationItemBinding.inflate(inflater, parent, false))
            else -> error("")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] is CreationItem.Chips) {
            CHIPS_VIEW_TYPE
        } else {
            CARD_VIEW_TYPE
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = data[position]) {
            is CreationItem.Chips -> {
                holder as ChipsViewHolder
                holder.chip0.setOnCheckedChangeListener(::chipClick)
                holder.chip1.setOnCheckedChangeListener(::chipClick)
                holder.chip2.setOnCheckedChangeListener(::chipClick)
                holder.chip3.setOnCheckedChangeListener(::chipClick)
                if (!item.loading) {
                    holder.skeleton.animate()
                        .alpha(0f)
                        .setInterpolator(DecelerateInterpolator())
                        .setDuration(500)
                        .start()
                }
            }

            is CreationItem.Card -> {
                holder as CardViewHolder
                holder.image.load(item.imageUrl) {
                    listener(onSuccess = { _, _ ->
                        holder.skeleton.animate()
                            .setStartDelay(listOf(100, 200, 300, 400, 500).random().toLong())
                            .alpha(0f)
                            .setInterpolator(DecelerateInterpolator())
                            .setDuration(500)
                            .start()
                    })
                }
                holder.title.text = item.title
                holder.userName.text = item.userName
                ViewCompat.setTransitionName(holder.image, "image_" + item.id)
                holder.binding.root.setOnClickListener {
                    clickListener(item, holder.binding)
                }
            }
        }
    }

    override fun getItemCount(): Int = data.size

    fun setData(creations: List<CreationItem>) {
        originalData.clear()
        originalData.addAll(creations)
        updateList(creations)
    }

    private fun chipClick(
        @Suppress("UNUSED_PARAMETER") chip: CompoundButton,
        isChecked: Boolean
    ) {
        if (isChecked) {
            originalData.filterIndexed { index, _ ->
                if (index == 0) {
                    true
                } else {
                    (0..2).random() == 1
                }
            }.let {
                updateList(it)
            }
        } else {
            updateList(originalData)
        }
    }

    private fun updateList(creations: List<CreationItem>) {
        val diffResult = DiffUtil.calculateDiff(DiffUtilCallback(data, creations))
        data.clear()
        data.addAll(creations)
        diffResult.dispatchUpdatesTo(this)
    }

    companion object {
        const val CHIPS_VIEW_TYPE = 0
        const val CARD_VIEW_TYPE = 1
    }

    inner class ChipsViewHolder(
        binding: ChipsItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        val group: ConstraintLayout = binding.root
        val skeleton: View = binding.root
        val chip0: Chip = binding.chip0
        val chip1: Chip = binding.chip1
        val chip2: Chip = binding.chip2
        val chip3: Chip = binding.chip3
    }

    inner class CardViewHolder(
        val binding: CreationItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        val skeleton: View = binding.skeleton
        val image: ImageView = binding.image
        val title: TextView = binding.title
        val userName: TextView = binding.userName
    }

    class DiffUtilCallback(
        private val oldList: List<CreationItem>,
        private val newList: List<CreationItem>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            newList[newItemPosition].position == oldList[oldItemPosition].position

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            newList[newItemPosition] == oldList[oldItemPosition]
    }
}