package fr.tutucorp.atelierde.ui.home.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat.startPostponedEnterTransition
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.Coil
import coil.load
import coil.request.ImageRequest
import com.google.android.material.carousel.MaskableFrameLayout
import fr.tutucorp.atelierde.databinding.CreationImageItemBinding
import fr.tutucorp.atelierde.databinding.CreationItemBinding
import fr.tutucorp.atelierde.ui.home.model.CreationItem

internal class CreationImageRecyclerViewAdapter(
    private val fragment: Fragment,
    private val id: String,
    data: List<String> = emptyList(),
    //private val clickListener: (CreationItem, CreationItemBinding) -> Unit
) : RecyclerView.Adapter<CreationImageRecyclerViewAdapter.ViewHolder>() {

    private val data: MutableList<String> = data.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CreationImageItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        if (position == 0) {
            ViewCompat.setTransitionName(holder.carouselItemContainer, "image_$id")
        }
        val request = ImageRequest.Builder(holder.image.context)
            .data(item)
            .crossfade(true)
            .target(onSuccess = {
                holder.image.setImageDrawable(it)
                if (position == 0) {
                    fragment.startPostponedEnterTransition()
                }
            })
            .build()
        Coil.imageLoader(holder.image.context).enqueue(request)
        /*holder.binding.root.setOnClickListener {
            clickListener(item, holder.binding)
        }*/
    }

    override fun getItemCount(): Int = data.size

    fun setData(images: List<String>) {
        val diffResult = DiffUtil.calculateDiff(DiffUtilCallback(data, images))
        data.clear()
        data.addAll(images)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class ViewHolder(binding: CreationImageItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val carouselItemContainer: MaskableFrameLayout = binding.carouselItemContainer
        val image: ImageView = binding.image
    }

    class DiffUtilCallback(
        private val oldList: List<String>,
        private val newList: List<String>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            newList[newItemPosition] == oldList[oldItemPosition]

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            newList[newItemPosition] == oldList[oldItemPosition]
    }
}