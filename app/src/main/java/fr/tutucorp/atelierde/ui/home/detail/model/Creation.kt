package fr.tutucorp.atelierde.ui.home.detail.model

data class Creation(
    val id: String,
    val title: String,
    val size: String,
    val fabric: String,
    val pattern: String,
    val userName: String,
    val imageUrls: List<String>
)