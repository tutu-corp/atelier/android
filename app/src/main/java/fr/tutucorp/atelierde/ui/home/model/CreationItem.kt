package fr.tutucorp.atelierde.ui.home.model

import java.util.UUID

sealed interface CreationItem {

    val id: String
    val position: Int
    val loading: Boolean

    data class Chips(
        override val id: String,
        override val position: Int,
        override val loading: Boolean = false
    ) : CreationItem

    data class Card(
        override val id: String,
        override val position: Int,
        override val loading: Boolean = false,
        val title: String,
        val userName: String,
        val imageUrl: String,
    ) : CreationItem {
        companion object {
            fun loading(position: Int) = Card(
                id = UUID.randomUUID().toString(),
                title = "loading",
                userName = "loading",
                imageUrl = "loading",
                loading = true,
                position = position
            )
        }
    }
}