package fr.tutucorp.atelierde.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.tutucorp.atelierde.data.MainRepository
import fr.tutucorp.atelierde.ui.home.model.CreationItem
import fr.tutucorp.atelierde.ui.home.model.HomeState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val loadingState = listOf(
        CreationItem.Chips("chips", loading = true, position = 0),
        CreationItem.Card.loading(position = 1),
        CreationItem.Card.loading(position = 2),
        CreationItem.Card.loading(position = 3),
        CreationItem.Card.loading(position = 4),
        CreationItem.Card.loading(position = 5),
        CreationItem.Card.loading(position = 6)
    )

    private val _state = MutableStateFlow<HomeState>(
        HomeState.Loading(creations = loadingState)
    )
    val state: StateFlow<HomeState>
        get() = _state

    init {
        viewModelScope.launch {
            delay(3_000)
            _state.value = HomeState.Success(
                creations = MainRepository.getInstance().getCreations()
            )
        }
    }
}
