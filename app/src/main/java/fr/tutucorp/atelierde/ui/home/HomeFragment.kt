package fr.tutucorp.atelierde.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialSharedAxis
import fr.tutucorp.atelierde.databinding.FragmentHomeBinding
import fr.tutucorp.atelierde.ui.home.CreationRecyclerViewAdapter.Companion.CHIPS_VIEW_TYPE
import fr.tutucorp.atelierde.ui.home.model.HomeState
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private lateinit var recyclerViewAdapter: CreationRecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = MaterialContainerTransform()

        exitTransition = MaterialSharedAxis(MaterialSharedAxis.Y, /* forward= */ true).apply {
            duration = 400
        }
        reenterTransition = MaterialSharedAxis(MaterialSharedAxis.Y, /* forward= */ false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val viewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect { state ->
                    when (state) {
                        is HomeState.Loading -> recyclerViewAdapter.setData(state.creations)
                        is HomeState.Success -> recyclerViewAdapter.setData(state.creations)
                    }

                }
            }
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()
        setupRecyclerView()
        binding.list.doOnPreDraw {
            startPostponedEnterTransition()
        }
    }

    private fun setupRecyclerView() {
        recyclerViewAdapter = CreationRecyclerViewAdapter(
            context = requireContext()
        ) { item, binding ->
            val extras = FragmentNavigatorExtras(binding.image to "image_" + item.id)
            findNavController().navigate(
                directions = HomeFragmentDirections.detailAction(item.id),
                navigatorExtras = extras
            )
        }
        binding.list.apply {
            itemAnimator = DefaultItemAnimator().apply {
                supportsChangeAnimations = false
            }
            adapter = recyclerViewAdapter
            (layoutManager as GridLayoutManager).spanSizeLookup = object : SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (recyclerViewAdapter.getItemViewType(position) == CHIPS_VIEW_TYPE) 2 else 1
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
