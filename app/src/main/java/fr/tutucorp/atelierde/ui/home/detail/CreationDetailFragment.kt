package fr.tutucorp.atelierde.ui.home.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.carousel.CarouselLayoutManager
import com.google.android.material.carousel.HeroCarouselStrategy
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialSharedAxis
import fr.tutucorp.atelierde.databinding.CreationDetailFragmentBinding

class CreationDetailFragment : Fragment() {

    private val args: CreationDetailFragmentArgs by navArgs()
    private var _binding: CreationDetailFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var recyclerViewAdapter: CreationImageRecyclerViewAdapter

    private lateinit var viewModel: CreationDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = MaterialContainerTransform().apply {
            scrimColor = 0x00000000
            duration = 700
        }
        enterTransition = MaterialSharedAxis(MaterialSharedAxis.Y, /* forward= */ true).apply {
            duration = 400
        }
        returnTransition = MaterialSharedAxis(MaterialSharedAxis.Y, /* forward= */ false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[CreationDetailViewModel::class.java]
        _binding = CreationDetailFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.topBar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        setupRecyclerView()

        /*ViewCompat.setOnApplyWindowInsetsListener(binding.textHome) { view, windowInsets ->
            val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
            view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                updateMargins(top = insets.top)
            }
            WindowInsetsCompat.CONSUMED
        }*/

        viewModel.getCreation(id = args.id).let { creation ->
            _binding?.apply {
                recyclerViewAdapter.setData(images = creation.imageUrls)
                title.text = creation.title
                description.text = listOf(
                    creation.pattern,
                    creation.size,
                    creation.fabric
                ).joinToString(separator = " · ")
            }
        }


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupRecyclerView() {
        recyclerViewAdapter = CreationImageRecyclerViewAdapter(fragment = this, id = args.id)
        binding.carouselRecyclerView.apply {
            adapter = recyclerViewAdapter
            layoutManager = CarouselLayoutManager(HeroCarouselStrategy()).apply {
                carouselAlignment = CarouselLayoutManager.ALIGNMENT_CENTER
            }
        }
    }
}
