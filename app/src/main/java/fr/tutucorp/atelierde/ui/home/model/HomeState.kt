package fr.tutucorp.atelierde.ui.home.model

sealed interface HomeState {
    data class Loading(val creations: List<CreationItem>) : HomeState

    data class Success(val creations: List<CreationItem>) : HomeState
}