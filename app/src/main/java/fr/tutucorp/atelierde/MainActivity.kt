package fr.tutucorp.atelierde

import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import fr.tutucorp.atelierde.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        binding.navView.setupWithNavController(navController)

        val slideOut = AnimationUtils.loadAnimation(this, R.anim.slide_out)
        val slideIn = AnimationUtils.loadAnimation(this, R.anim.slide_in)
        var isHidden = false
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.creationDetailFragment) {
                isHidden = true
                binding.navView.startAnimation(slideOut)
            } else if (isHidden) {
                isHidden = false
                binding.navView.startAnimation(slideIn)
            }
        }
    }
}