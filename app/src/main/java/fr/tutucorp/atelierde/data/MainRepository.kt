package fr.tutucorp.atelierde.data

import fr.tutucorp.atelierde.ui.home.detail.model.Creation
import fr.tutucorp.atelierde.ui.home.model.CreationItem

class MainRepository private constructor() {

    private val creations = mutableListOf(
        CreationItem.Chips(id = "chips", position = 0),
        CreationItem.Card(
            id = "1",
            title = "Petit bob",
            userName = "Hugo",
            position = 1,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob1.imageset/crea-bob1.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "2",
            title = "Bob d'hiver",
            userName = "Tulishandria",
            position = 2,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob2.imageset/crea-bob2.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "3",
            title = "Veste",
            userName = "Tulishandria",
            position = 3,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-gilet1.imageset/crea-gilet1.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "4",
            title = "Petit bob",
            userName = "Hugo",
            position = 4,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob1.imageset/crea-bob1.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "5",
            title = "Bob d'hiver",
            userName = "Tulishandria",
            position = 5,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob2.imageset/crea-bob2.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "6",
            title = "Veste",
            userName = "Tulishandria",
            position = 6,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-gilet1.imageset/crea-gilet1.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "7",
            title = "Petit bob",
            userName = "Hugo",
            position = 7,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob1.imageset/crea-bob1.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "8",
            title = "Bob d'hiver",
            userName = "Tulishandria",
            position = 8,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob2.imageset/crea-bob2.jpeg?ref_type=heads"
        ),
        CreationItem.Card(
            id = "9",
            title = "Veste",
            userName = "Tulishandria",
            position = 9,
            imageUrl = "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-gilet1.imageset/crea-gilet1.jpeg?ref_type=heads"
        )
    )

    fun getCreations(): MutableList<CreationItem> = creations

    fun getCreation(id: String): Creation? = creations.find { it.id == id }?.let {
        it as CreationItem.Card
        Creation(
            id = it.id,
            title = it.title,
            pattern = "Gilet",
            size = "12 ans",
            fabric = "Popeline de coton",
            userName = it.userName,
            imageUrls = listOf(
                it.imageUrl,
                "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob1.imageset/crea-bob1.jpeg?ref_type=heads",
                "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-bob2.imageset/crea-bob2.jpeg?ref_type=heads",
                "https://gitlab.com/tutu-corp/atelier/ios/-/raw/main/atelierde/Assets.xcassets/Images/post/crea-gilet1.imageset/crea-gilet1.jpeg?ref_type=heads"
            )
        )
    }

    companion object {

        private var instance: MainRepository? = null

        fun getInstance() = instance ?: MainRepository().also {
            instance = it
        }
    }
}